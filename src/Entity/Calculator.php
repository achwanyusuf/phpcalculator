<?php
/**
 * @Entity @Table(name="calculator")
 */
class Calculator
{
    /** @Column(type="string") */
    protected $command;
    /** @Column(type="string") */
    protected $description;
    /** @Column(type="string") */
    protected $result;
    /** @Column(type="string") */
    protected $output;
    /** @Column(type="datetime") */
    protected $time;

    public function getCommand()
    {
        return $this->command;
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function getResult()
    {
        return $this->result;
    }
    
    public function getOutput()
    {
        return $this->output;
    }
    
    public function getTime()
    {
        return $this->time;
    }

    public function setCommand($command)
    {
        $this->command = $command;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }
    
    public function setResult($result)
    {
        $this->result = $result;
    }
    
    public function setOutput($output)
    {
        $this->output = $output;
    }
    
    public function setTime($time)
    {
        $this->time = $time;
    }
}