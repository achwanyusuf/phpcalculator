<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:clear';
    protected $commandHistoryManager;

    /**
     * @var string
     */
    protected $description = 'Clear saved history';

    public function __construct(CommandHistoryManagerInterface $commandHistoryManager)
    {
        $this->commandHistoryManager = $commandHistoryManager;
        parent::__construct();
    }

    public function handle(): void
    {
        $res = $this->commandHistoryManager->clearAll();
        if($res){
            printf('History cleared!');
        } else {
            printf('Failed to clear history!');
        }
    }
}
?>
