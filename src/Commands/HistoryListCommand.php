<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryListCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:list {commands?* : Filter the history by commands} {--D|driver=database : Driver for storage connection}';
    protected $commandHistoryManager;

    /**
     * @var string
     */
    protected $description = 'Show calculator history';

    public function __construct(CommandHistoryManagerInterface $commandHistoryManager)
    {
        $this->commandHistoryManager = $commandHistoryManager;
        parent::__construct();
    }

    public function handle(): void
    {
        $commands = $this->getCommands();
        $options = $this->getOptions();
        if($options['driver'] != 'database' && $options['driver'] != 'file'){
            print_r('Driver not found!');
            exit();
        }
        $this->commandHistoryManager->findAll($commands, $options);
    }

    protected function getCommands(): array
    {
        if($this->argument('commands')){
            return $this->argument('commands');
        } else {
            return [];
        }
    }

    protected function getOptions(): array
    {
        return $this->options();
    }
}
?>
