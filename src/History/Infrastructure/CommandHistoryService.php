<?php
namespace Jakmall\Recruitment\Calculator\History\Infrastructure;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Jakmall\Recruitment\Calculator\History\DBAL\HistoryController;

class CommandHistoryService implements CommandHistoryManagerInterface {
    protected $filesystem;
    protected $historyController;

    public function __construct(HistoryController $historyController)
    {
        $this->filesystem = new Filesystem();
        $this->historyController = $historyController;
    }
    public function findAll($commands, $options): array{
        return $this->historyController->generateHistory($commands, $options);
    }
    public function log($command, $description, $result, $output, $time): bool{
        $this->saveToFile($command, $description, $result, $output, $time);
        $this->saveToDatabase($command, $description, $result, $output, $time);
        return true;
    }
    public function clearAll():bool{
        return $this->historyController->clearAll();
    }

    function saveToFile($command, $description, $result, $output, $time): void{
        $previousData = file_get_contents('history.json');
        $resultData = json_decode($previousData);
        if(!$resultData){
            $resultData = [];
        }
        $data = (object) array("command" => $command, "description" => $description, "result" => $result, "output" => $output, "time"=> $time);
        array_push($resultData, $data);
        $this->filesystem->dumpFile('history.json', json_encode($resultData) . "\n");
    }

    function saveToDatabase($command, $description, $result, $output, $time): void{
        $this->historyController->addToDB($command, $description, $result, $output, $time);
    }
}