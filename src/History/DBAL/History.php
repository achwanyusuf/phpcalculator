<?php

namespace Jakmall\Recruitment\Calculator\History\DBAL;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Connection;
use LucidFrame\Console\ConsoleTable;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class HistoryController
{  
    protected $connection;
    protected $filesystem;

    public function __construct()
    {   
        $connectionParams = array(
            'url' => $_ENV['DATABASE_URL'],
            'user' => $_ENV['DATABASE_USER'],
            'password' => $_ENV['DATABASE_PASSWORD'],
            'driver' => $_ENV['DATABASE_DRIVER'],
        );
        try {
            $connection = DriverManager::getConnection($connectionParams, new Configuration());
            $this->connection = $connection;
        }catch(Exception $e){
            echo $e->getMessage();
        }
        $this->filesystem = new Filesystem();
    }

    public function addToDB($command, $description, $result, $output, $time){
        try{
            $this->connection->insert('calculator', array('command' => $command, 'description' => $description, 'result' => $result, 'output' => $output, 'time' => $time));
            return true;
        } catch(Exception $e){
            echo $e->getMessage();
            return false;
        }
    }

    public function generateHistory($command, $options){
        if($options['driver'] == 'database'){
            return $this->generateByDB($command);
        } else if($options['driver'] == 'file'){
            return $this->generateByFile($command);
        }
    }

    public function generateByFile($command){
        $previousData = file_get_contents('history.json');
        $resultData = json_decode($previousData);
        if(!$resultData){
            $resultData = [];
        }
        if($command != []){
            $data = $resultData;
            $resultData = [];
            foreach ($data as $value){
                if(in_array($value->command, $command)){
                    array_push($resultData, $value);
                }
            }
            $resultData = json_decode(json_encode($resultData));
        }
        $this->display($resultData);
    }

    public function generateByDB($command){
        $result = [];
        if($command == []){
            $stmt = $this->connection->prepare('SELECT command,description,result,output,time FROM calculator');
            $stmt->execute();
        } else {
            $stmt = $this->connection->executeQuery('SELECT command,description,result,output,time FROM calculator WHERE command IN (?)', array($command), array(Connection::PARAM_STR_ARRAY));
        }
        while ($row = $stmt->fetch()) {
            array_push($result, $row);
        }
        $result = json_decode(json_encode($result));
        $this->display($result);
    }

    public function display($data){
        $tbl = new ConsoleTable();
        if($data == []){
            printf('History is empty').
            exit();
        }
        $tbl->addHeader('No')->addHeader('Command')->addHeader('Description')->addHeader('Result')->addHeader('Output')->addHeader('Time');
        foreach ($data as $key => $value){
            $tbl->addRow()
                ->addColumn($key+1)
                ->addColumn($value->command)
                ->addColumn($value->description)
                ->addColumn($value->result)
                ->addColumn($value->output)
                ->addColumn($value->time);
        }
        $tbl->display();
    }

    public function clearAll(){
        try {
            $this->filesystem->dumpFile('history.json', json_encode([]) . "\n");
        }catch(Exception $e){
            echo $e->getMessage();
            return false;
        }
        try {
            $stmt = $this->connection->prepare('DELETE FROM calculator');
            $stmt->execute();
            return true;
        }catch(Exception $e){
            echo $e->getMessage();
            return false;
        }
        return false;
    }
}